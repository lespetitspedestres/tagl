#![cfg_attr(debug_assertions, windows_subsystem = "console")]
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

fn main() {
    gooey::init();

    if let Err(e) = tagl::launch() {
        if cfg!(debug_assertions) {
            eprintln!("{}", e);
        } else {
            gooey::alert(&format!("Fatal error!\n{}", e))
        }
    }
}
