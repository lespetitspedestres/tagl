use fltk::image::BmpImage;
use log::warn;
use rust_embed::RustEmbed;

#[derive(RustEmbed)]
#[folder = "src/view/icons"]
#[include = "*.bmp"]
struct Icons;

pub fn load(name: &str) -> Option<BmpImage> {
    if let Some(file) = Icons::get(name) {
        match BmpImage::from_data(&file.data) {
            Err(error) => {
                warn!("Failed to load icon: {}", error);
                None
            }
            Ok(image) => Some(image),
        }
    } else {
        warn!("Unknown icon {}", name);
        None
    }
}
