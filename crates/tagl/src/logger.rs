use env_logger::{Target, WriteStyle};
use log::LevelFilter;
use std::{fs::File, path::Path};

pub fn init(path: &Path) -> anyhow::Result<()> {
    let file = File::create(path)?;
    env_logger::builder()
        .write_style(WriteStyle::Always)
        .filter_level(LevelFilter::Trace)
        .format_level(true)
        .format_target(true)
        .format_timestamp_secs()
        .target(Target::Pipe(Box::new(file)))
        .try_init()?;
    Ok(())
}
