use crate::{MAGIC, ZSTD_WINDOW_LOG};
use integer_encoding::VarIntReader;
use log::{debug, warn};
use std::io::{Cursor, Error, ErrorKind, Read, Result};
use windows::{fs, fs::File, path::Path};
use zstd::Decoder;

// TODO should return script?
pub fn extract(source: &Path, target: &Path) -> Result<()> {
    let file = File::open(source)?;
    let file = file.map()?;
    let file = file.view()?;
    debug!("File size: {}", file.len());

    let mut cursor = Cursor::new(file);

    let mut magic = [0; MAGIC.len()];
    cursor.read_exact(&mut magic)?;
    if magic != MAGIC {
        warn!("Invalid magic number");
        return Err(Error::from(ErrorKind::InvalidData));
    }

    // NOTE: ZSTD Decoder has an internal buffer for its inner reader
    let mut decoder = Decoder::new(cursor)?;
    decoder.window_log_max(ZSTD_WINDOW_LOG)?;

    let mut buffer = Vec::new();

    let script = read_script(&mut decoder, &mut buffer)?;
    debug!("{}", script);

    let _ = fs::create_dir(target);

    loop {
        let path = match read_path(&mut decoder, &mut buffer) {
            Ok(path) => path,
            Err(e) if e.kind() == ErrorKind::UnexpectedEof => break,
            Err(e) => return Err(e),
        };
        let path = target.join(&path);
        debug!("Extracting {}", path.display());

        if path.exists() {
            warn!("File {} already exists", path.display());
            continue;
        }

        read_file(&mut decoder, &path)?;
        buffer.clear();
    }

    debug!("End of archive");
    Ok(())
}

fn read_script<'a, R: Read>(reader: &mut R, buffer: &'a mut Vec<u8>) -> Result<&'a str> {
    let size: u32 = reader.read_varint()?;
    buffer.clear();
    buffer.resize(size as usize, 0);
    reader.read_exact(buffer)?;
    let script = std::str::from_utf8(buffer).map_err(|e| Error::new(ErrorKind::InvalidData, e))?;
    Ok(script)
}

fn read_path<'a, R: Read>(reader: &mut R, buffer: &'a mut Vec<u8>) -> Result<&'a Path> {
    buffer.clear();
    loop {
        let mut code = [0; 2];
        reader.read_exact(&mut code)?;
        buffer.extend_from_slice(&code);
        let code = u16::from_le_bytes(code);
        if code == 0 {
            break;
        }
    }
    let slice = bytemuck::try_cast_slice(buffer.as_slice()).map_err(|_| Error::from(ErrorKind::InvalidData))?;
    Path::from_slice(slice).map_err(|_| Error::from(ErrorKind::InvalidData))
}

fn read_file<R: Read>(reader: &mut R, target: &Path) -> Result<()> {
    let mut size: u64 = reader.read_varint()?;

    // Create directories if needed
    if let Some(parent) = target.parent() {
        fs::create_dir_all(&parent)?;
    }

    // Create file new file mapped into memory
    debug!("Creating file {}", target.display());
    let mut file = File::create(target)?;
    file.truncate(size as _)?;
    let mut file = file.map_mut()?;
    let mut file = file.view_mut()?;
    let mut buffer = &mut *file;

    while size > 0 {
        // NOTE: Decode directly into the file view
        let n = reader.read(buffer)?;
        if n == 0 {
            return Err(Error::from(ErrorKind::UnexpectedEof));
        }
        buffer = &mut buffer[n..];
        size -= n as u64;
    }
    Ok(())
}
