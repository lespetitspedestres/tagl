use crate::manifest::{Manifest, Package};
use bitflags::bitflags;
use log::{debug, warn};
use serde::{Deserialize, Serialize};
use std::{cmp::Ordering, collections::HashMap, fs::File, path::Path};
use uuid::Uuid;

#[derive(Debug)]
pub struct Inventory {
    path: Box<Path>,
    inner: Inner,
}

#[derive(Debug, Deserialize, Serialize)]
struct Inner {
    /// The local list of packages.
    packages: HashMap<Uuid, Package>,
    /// The state of local packages.
    states: HashMap<Uuid, State>,
}

bitflags! {
    #[derive(Clone, Copy, Debug, PartialEq, Eq, Deserialize, Serialize)]
    #[serde(rename_all = "lowercase")]
    pub struct State: u32 {
        /// The package can be updated, a new version is available.
        const Updatable = 0b00000001;
        /// The package has been downloaded, the archive is available locally.
        const Downloaded = 0b00000010;
        /// The package has been extracted and configured.
        const Installed = 0b00000100;
    }
}

impl Inventory {
    pub fn iter(&self) -> impl Iterator<Item = (&'_ Package, &'_ State)> + '_ {
        self.inner.packages.iter().map(|(uuid, package)| {
            let state = self.inner.states.get(uuid).unwrap();
            (package, state)
        })
    }

    pub fn package(&self, uuid: Uuid) -> &Package {
        self.inner.packages.get(&uuid).unwrap()
    }

    pub fn state(&self, uuid: Uuid) -> &State {
        self.inner.states.get(&uuid).unwrap()
    }

    pub fn state_mut(&mut self, uuid: Uuid) -> &mut State {
        self.inner.states.get_mut(&uuid).unwrap()
    }

    pub fn update(&mut self, manifest: &Manifest) {
        for package in manifest.packages.iter() {
            if let Some(local) = self.inner.packages.get_mut(&package.uuid) {
                match package.version.cmp(&local.version) {
                    Ordering::Greater => {
                        debug!("newer version of package: {:?}", &package);
                        local.version = package.version; // TODO not sure
                        self.inner
                            .states
                            .get_mut(&package.uuid)
                            .unwrap()
                            .insert(State::Updatable);
                    }
                    Ordering::Less => {
                        warn!("older version of package {:?}", &package);
                    }
                    Ordering::Equal => {
                        debug!("package is up-to-date: {:?}", &package);
                    }
                }
            } else {
                debug!("new package: {:?}", &package);
                assert!(self.inner.packages.insert(package.uuid, package.clone()).is_none());
                assert!(self.inner.states.insert(package.uuid, State::empty()).is_none());
            }
        }
    }

    pub fn save(&self) -> serde_yaml::Result<()> {
        let file = File::create(&self.path).unwrap();
        serde_yaml::to_writer(file, &self.inner)
    }

    pub fn try_load(path: &Path) -> serde_yaml::Result<Option<Self>> {
        if let Ok(file) = File::open(path) {
            serde_yaml::from_reader(file).map(|inner| {
                Some(Self {
                    path: path.into(),
                    inner,
                })
            })
        } else {
            Ok(None)
        }
    }

    pub fn new(path: &Path, manifest: Manifest) -> Self {
        let packages = HashMap::from_iter(manifest.packages.iter().map(|package| (package.uuid, package.clone())));
        let states = HashMap::from_iter(manifest.packages.iter().map(|package| (package.uuid, State::empty())));
        Self {
            path: path.into(),
            inner: Inner { packages, states },
        }
    }
}
