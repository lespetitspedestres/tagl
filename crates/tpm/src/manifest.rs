//! # Repository
//!
//! ## Hierarchy
//!
//! ```text
//! .
//! ├── manifest.yml
//! ├── launcher-<VERSION>.tar.zst
//! ├── launcher-<VERSION>.tar.zst.b3s
//! ├── ...
//! └── packages
//!     ├── <UUID>-<VERSION>.tar.zst
//!     ├── <UUID>-<VERSION>.tar.zst.b3s
//!     ├── <UUID>-<VERSION>.yml
//!     └── ...
//! ```
//!
//! The package setup file (.yml) is not archived so that it can be modified
//! without recompressing all game files.
//!
//! The package archive name contains the version number to allow keeping old versions.
//!
//! `.b3s` files are BLAKE3 checksums
//!
//! ## Manifest
//!
//! ```text
//! version: 1.2.3
//! packages:
//!   - title: SuperTuxKart
//!     uuid: b4325d8e-4278-4e55-a58c-b96bd3931e65
//!     version: 1.2.3
//!   - title: Teeworlds
//!     uuid: b7a53ee9-90f4-4b7b-be2e-d648a0161b65
//!     version: 1.2.3
//! ```
//!
//! ## Package script (config file)
//!
//! `<UUID>-<VERSION>.yml`
//!
//! ```text
//! executable: foo/bar.exe
//! ```
//!

use crate::version::Version;
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use uuid::Uuid;

/// A Manifest describes the content of a repository.
/// It contains the list of packages.
#[derive(Debug, Deserialize, Serialize)]
pub struct Manifest {
    /// List of packages
    pub packages: Box<[Package]>,
}

impl FromStr for Manifest {
    type Err = serde_yaml::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        serde_yaml::from_str(s)
    }
}

/// Description of a package.
#[derive(Debug, Deserialize, Serialize, Clone)]
pub struct Package {
    pub title: Box<str>,
    /// The unique identifier of the package.
    pub uuid: Uuid,
    /// Version of the package.
    pub version: Version,
}
