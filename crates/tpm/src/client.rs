use crate::repository::{Credentials, Repository};
use blake3::{Hash, Hasher};
use curl::easy::{Auth, Easy};
use log::{debug, error};
use std::{fs::File, io, io::Write, path::Path, str};

#[derive(Debug)]
pub struct Client {
    agent: Easy,
    root: Box<str>,
}

impl Client {
    pub fn new(repository: Repository) -> io::Result<Self> {
        let mut agent = Easy::new();
        agent.fail_on_error(true)?;

        if let Some(Credentials::Basic { username, password }) = repository.credentials {
            agent.http_auth(Auth::new().basic(true))?;
            agent.username(&username)?;
            agent.password(&password)?;
        }

        Ok(Self {
            agent,
            root: repository.url,
        })
    }

    /// Downloads data at the given URL into a file.
    pub fn download<P>(&mut self, url: &str, path: &Path, mut progress: P) -> io::Result<Option<Hash>>
    where
        P: FnMut(f64, u64) -> bool,
    {
        let url = format!("{}/{}", &self.root, url);
        debug!("URL={}", url);
        debug!("PATH={}", path.to_string_lossy());

        let mut file = File::create(path)?;
        let mut hasher = Hasher::new();

        self.agent.url(&url)?;
        self.agent.progress(true)?;

        // Fetch package file (with progress callback)
        let result = {
            let mut transfer = self.agent.transfer();

            transfer.progress_function(move |bytes_total, bytes_now, _, _| {
                progress(bytes_now / bytes_total, bytes_now as u64)
            })?;

            transfer.write_function(|data| {
                hasher.update(data);
                Ok(file.write(data).unwrap_or_else(|e| {
                    error!("{}", e);
                    0
                }))
            })?;

            transfer.perform()
        };

        match result {
            Err(e) if e.is_aborted_by_callback() => Ok(None),
            Err(e) => Err(e.into()),
            Ok(()) => Ok(Some(hasher.finalize())),
        }
    }

    /// Reads data at the given URL into a String.
    pub fn read_to_string(&mut self, url: &str) -> io::Result<String> {
        let url = format!("{}/{}", &self.root, url);
        debug!("URL={}", url);

        self.agent.url(&url)?;

        let mut buffer = Vec::new();
        {
            let mut transfer = self.agent.transfer();
            transfer.write_function(|data| {
                buffer.extend_from_slice(data);
                Ok(data.len())
            })?;
            transfer.perform()?;
        }

        String::from_utf8(buffer).map_err(|e| {
            error!("{}", e);
            io::ErrorKind::InvalidData.into()
        })
    }
}
