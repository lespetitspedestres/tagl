#![forbid(unsafe_code)]
#![warn(missing_debug_implementations)]
#![cfg(windows)]

pub mod client;
pub mod config;
pub mod inventory;
pub mod manager;
pub mod manifest;
pub mod repository;
pub mod script;
pub mod template;

mod version;

pub const VERSION: &str = env!("CARGO_PKG_VERSION");
