use serde::{Deserialize, Serialize};
use serde_yaml::Result;
use std::fs::File;

#[derive(Debug, Deserialize, Serialize)]
pub struct Repository {
    pub url: Box<str>,
    pub credentials: Option<Credentials>,
}

impl Repository {
    pub fn load(data: &str) -> Result<Self> {
        serde_yaml::from_str(data)
    }

    pub fn save(&self, file: File) -> Result<()> {
        serde_yaml::to_writer(file, self)
    }
}

#[derive(Debug, Deserialize, Serialize)]
#[serde(rename_all = "lowercase")]
pub enum Credentials {
    Basic { username: Box<str>, password: Box<str> },
}
