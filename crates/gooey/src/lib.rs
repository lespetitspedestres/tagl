//! The GUI for TAGL
#![deny(missing_debug_implementations)]

pub mod setup;
pub mod view;

use fltk::app::Scheme;
use log::debug;

pub fn init() {
    debug!("FLTK v{}", fltk::app::crate_version());
    fltk::app::init_all();
    fltk::app::set_scheme(Scheme::Gtk);
}

pub fn alert(txt: &str) {
    fltk::dialog::alert(0, 0, txt);
}
