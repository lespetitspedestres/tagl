use crossbeam_channel::{Receiver, Sender, TryRecvError};
use fltk::{
    app,
    button::{Button, RadioButton},
    dialog,
    enums::{Color, Event as FltkEvent, FrameType, Key},
    frame::Frame,
    group::{Pack, PackType, Row, Scroll},
    misc::Progress,
    prelude::*,
    window::Window,
};
use log::{debug, error, trace};
use std::{cell::Cell, io, rc::Rc, time::Instant};
use tpm::{inventory::State, manifest::Package};
use uuid::Uuid;

const TITLE: &str = "TAGL - The Amazing Game Launcher";
const WINDOW_WIDTH: i32 = 600;
const WINDOW_HEIGHT: i32 = 300;

#[derive(Debug)]
pub struct Entry {
    package: Package,
    state: State,
}

impl Entry {
    pub fn new(package: Package, state: State) -> Self {
        Self { package, state }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Action {
    Upgrade,
    InventoryUpdate,
    PackageDownload { uuid: Uuid },
    PackageDownloadCancel,
    PackageDelete { uuid: Uuid },
    PackageInstall { uuid: Uuid },
    PackageUninstall { uuid: Uuid },
    PackageExecute { uuid: Uuid },
    Exit,
}

#[derive(Debug)]
pub enum Event {
    InventoryInit { entries: Box<[Entry]> },
    InventoryUpdate { result: io::Result<Box<[Entry]>> },
    PackageDownloadBegin { title: Box<str> },
    PackageDownloadProgress { ratio: f64, speed: u64 },
    PackageDownloadCancel,
    PackageDownloadEnd { result: io::Result<bool> },
    PackageDeleteEnd { result: io::Result<bool> },
    PackageInstallBegin,
    PackageInstallEnd { result: io::Result<()> },
    PackageUninstallBegin,
    PackageUninstallEnd { result: io::Result<()> },
    PackageExecute { result: io::Result<bool> },
}

fn ask_exit(actions: &Sender<Action>) {
    if let Some(0) = dialog::choice2_default("Are you sure you want to exit?", "Yes", "No", "") {
        actions.send(Action::Exit).unwrap();
        app::quit();
    }
}

pub fn show(actions: Sender<Action>, events: Receiver<Event>) {
    let mut window = Window::default()
        .with_size(WINDOW_WIDTH, WINDOW_HEIGHT)
        .with_label(TITLE)
        .center_screen();
    window.begin();

    let pack = Pack::default().size_of_parent();
    pack.begin();

    let selection = Rc::new(Cell::new(None));

    /* Header bar */
    {
        const HEIGHT: i32 = 40;

        let mut pack = Pack::default().with_size(WINDOW_WIDTH, HEIGHT);
        pack.set_type(PackType::Horizontal);
        pack.set_spacing(4);

        let mut bt_update = Button::default().with_size(HEIGHT, HEIGHT);
        bt_update.set_tooltip("Update manifest");
        bt_update.set_label("@#+3refresh");

        let mut bt_upgrade = Button::default().with_size(HEIGHT, HEIGHT);
        bt_upgrade.set_tooltip("Upgrade launcher");
        bt_upgrade.set_label("@#+3reload");

        pack.end();

        bt_upgrade.set_callback({
            let actions = actions.clone();
            move |_| actions.send(Action::Upgrade).unwrap()
        });

        bt_update.set_callback({
            let actions = actions.clone();
            move |_| actions.send(Action::InventoryUpdate).unwrap()
        });
    }

    const ITEM_HEIGHT: i32 = 40;

    let mut package_list;
    {
        const HEIGHT: i32 = 200;
        const SCROLLBAR_SIZE: i32 = 16;

        let mut scroll = Scroll::default().with_size(window.width(), HEIGHT);
        scroll.set_scrollbar_size(SCROLLBAR_SIZE);
        scroll.begin();
        package_list = Pack::default().with_size(window.width() - SCROLLBAR_SIZE, HEIGHT);
        scroll.end();
    }

    /* Actions buttons */
    {
        const WIDTH: i32 = 80;
        const HEIGHT: i32 = 40;

        let mut pack = Pack::default().with_size(window.width(), HEIGHT);
        pack.set_type(PackType::Horizontal);
        pack.set_spacing(4);

        let mut bt_download = Button::default().with_size(WIDTH, HEIGHT).with_label("Download");
        let mut bt_delete = Button::default().with_size(WIDTH, HEIGHT).with_label("Delete");
        let mut bt_install = Button::default().with_size(WIDTH, HEIGHT).with_label("Install");
        let mut bt_uninstall = Button::default().with_size(WIDTH, HEIGHT).with_label("Uninstall");
        let mut bt_execute = Button::default().with_size(WIDTH, HEIGHT).with_label("Execute");

        let mut frame = Frame::default().with_size(WIDTH, HEIGHT).with_label("TODO");
        frame.set_frame(FrameType::DownBox);

        pack.end();

        // TODO enable/disable buttons based on package state
        // TODO could merge "opposite" buttons: install/uninstall, download/delete...

        bt_download.set_callback({
            let selection = Rc::clone(&selection);
            let actions = actions.clone();
            move |_| {
                if let Some(uuid) = selection.get() {
                    actions.send(Action::PackageDownload { uuid }).unwrap()
                }
            }
        });
        bt_delete.set_callback({
            let selection = Rc::clone(&selection);
            let actions = actions.clone();
            move |_| {
                if let Some(uuid) = selection.get() {
                    actions.send(Action::PackageDelete { uuid }).unwrap()
                }
            }
        });
        bt_install.set_callback({
            let selection = Rc::clone(&selection);
            let actions = actions.clone();
            move |_| {
                if let Some(uuid) = selection.get() {
                    actions.send(Action::PackageInstall { uuid }).unwrap()
                }
            }
        });
        bt_uninstall.set_callback({
            let selection = Rc::clone(&selection);
            let actions = actions.clone();
            move |_| {
                if let Some(uuid) = selection.get() {
                    actions.send(Action::PackageUninstall { uuid }).unwrap()
                }
            }
        });
        bt_execute.set_callback({
            let selection = Rc::clone(&selection);
            let actions = actions.clone();
            move |_| {
                if let Some(uuid) = selection.get() {
                    actions.send(Action::PackageExecute { uuid }).unwrap()
                }
            }
        });
    }

    pack.end();
    window.end();

    window.show();

    /* Package download window */

    let mut download_window = Window::default().with_size(320, 120).center_screen();
    let mut download_title;
    let mut download_time;
    let mut download_speed;
    let mut download_progress;

    {
        download_window.begin();

        download_title = Frame::default();
        download_time = Frame::default();
        download_speed = Frame::default();
        download_progress = Progress::default().with_size(120, 40);

        let mut pack = Pack::default().size_of_parent();
        pack.set_spacing(4);

        let mut row = Row::default().with_size(100, 40);
        row.add(&download_title);
        row.add(&download_time);
        row.add(&download_speed);
        row.end();

        download_progress.set_minimum(0.0);
        download_progress.set_maximum(1.0);

        pack.auto_layout();
        pack.end();

        download_window.end();
        download_window.make_modal(true);
    }

    {
        let actions = actions.clone();
        window.set_callback(move |_| {
            if app::event() == FltkEvent::Close {
                ask_exit(&actions);
            }
        });
    }

    {
        window.handle(move |_, event| match event {
            FltkEvent::KeyUp => {
                if app::event_key() == Key::Escape {
                    ask_exit(&actions);
                    return true;
                }
                false
            }
            _ => false,
        });
    }

    /* Event loop */

    let mut start = Instant::now();

    while app::wait() {
        let event = match events.try_recv() {
            Ok(event) => {
                trace!("{:?}", event);
                event
            }
            Err(TryRecvError::Empty) => continue,
            Err(TryRecvError::Disconnected) => {
                debug!("Done");
                break;
            }
        };
        match event {
            Event::InventoryInit { mut entries }
            | Event::InventoryUpdate {
                result: Ok(mut entries),
            } => {
                package_list.clear();

                // Sort entries by package title in alphabetical order
                entries.sort_unstable_by(|e1, e2| e1.package.title.cmp(&e2.package.title));

                // Create a button for each package entry
                for entry in entries.iter() {
                    let label = format!("{} - v{}", &entry.package.title, entry.package.version);
                    let mut button = RadioButton::default().with_size(80, ITEM_HEIGHT).with_label(&label);
                    if entry.package.version.revision == 0 {
                        button.deactivate();
                    }
                    if entry.state.contains(State::Updatable) {
                        button.set_color(Color::Red);
                    } else if entry.state.contains(State::Installed) {
                        button.set_color(Color::Green);
                    }
                    button.set_selection_color(Color::Dark2);

                    let uuid = entry.package.uuid;
                    let selection = Rc::clone(&selection);
                    button.set_callback(move |_| {
                        trace!("Selecting package {}", uuid);
                        selection.set(Some(uuid));
                    });
                    package_list.add(&button);
                }
            }
            Event::InventoryUpdate { result: Err(error) } => {
                error!("Failed to update manifest: {}", error);
                dialog::alert_default("Failed to update manifest");
            }
            Event::PackageDownloadBegin { title } => {
                download_title.set_label(&title);
                download_window.show();
                start = Instant::now();
            }
            Event::PackageDownloadProgress { ratio, speed } => {
                const KIB: u64 = 1024;
                const MIB: u64 = KIB * KIB;
                const GIB: u64 = MIB * MIB;

                let speed = if speed >= GIB {
                    format!("{:.2} GiB/s", speed as f64 / GIB as f64)
                } else if speed >= MIB {
                    format!("{:.2} MiB/s", speed as f64 / MIB as f64)
                } else {
                    format!("{:.2} KiB/s", speed as f64 / KIB as f64)
                };
                download_speed.set_label(&speed);

                let elapsed = start.elapsed().as_secs();
                let time = format!("{:02}:{:02}", elapsed / 60, elapsed % 60);
                download_time.set_label(&time);

                download_progress.set_value(ratio);
                download_progress.set_label(&format!("{}%", (ratio * 100.0) as u32));
            }
            Event::PackageDownloadCancel => {
                download_window.hide();
            }
            Event::PackageDownloadEnd { result } => {
                download_window.hide();

                match result {
                    Ok(true) => dialog::message_default("Download success"),
                    Ok(false) => dialog::message_default("Download cancelled"),
                    Err(error) => {
                        error!("Failed to download package: {}", error);
                        dialog::alert_default("Failed to download package");
                    }
                }
            }
            Event::PackageInstallBegin => todo!(),
            Event::PackageInstallEnd { result } => match result {
                Ok(_) => {
                    dialog::message_default("Package successfully installed!");
                }
                Err(error) => {
                    error!("Failed to install package: {}", error);
                    dialog::alert_default("Failed to install package!")
                }
            },
            Event::PackageUninstallBegin => todo!(),
            Event::PackageUninstallEnd { result } => match result {
                Ok(_) => {
                    dialog::message_default("Package successfully uninstalled!");
                }
                Err(error) => {
                    error!("Failed to uninstall package: {}", error);
                    dialog::alert_default("Failed to uninstall package")
                }
            },
            Event::PackageDeleteEnd { result } => match result {
                Ok(true) => dialog::message_default("Package successfully deleted!"),
                Ok(false) => dialog::message_default("Package is not downloaded!"),
                Err(error) => {
                    error!("Failed to delete package: {}", error);
                    dialog::alert_default("Failed to delete package")
                }
            },
            Event::PackageExecute { result } => match result {
                Ok(true) => { /* Nothing */ }
                Ok(false) => dialog::message_default("Failed to execute!"),
                Err(error) => {
                    error!("Failed to execute package: {}", error);
                    dialog::alert_default("Failed to execute package")
                }
            },
        }
        app::redraw();
    }
}

pub fn notify(events: &Sender<Event>, event: Event) {
    events.send(event).unwrap();
    app::awake();
}
