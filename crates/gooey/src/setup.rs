use fltk::{
    app,
    button::Button,
    dialog,
    enums::FrameType,
    frame::Frame,
    group::{Flex, Pack, PackType},
    input::{FileInput, Input, IntInput, SecretInput},
    menu::Choice,
    prelude::*,
    window::Window,
};
use log::trace;
use std::{cell::Cell, path::Path, rc::Rc};
use tpm::{
    config::{Config, Hardware, Screen, User},
    repository::{Credentials, Repository},
};

const WINDOW_WIDTH: i32 = 600;
const WINDOW_HEIGHT: i32 = 600;

pub fn show(default_install_path: &Path) -> Option<(Repository, Config)> {
    let output = Rc::new(Cell::new(None));
    let mut window = Window::default().with_size(WINDOW_WIDTH, WINDOW_HEIGHT).center_screen();

    window.begin();
    let mut top = Flex::default().size_of_parent().column();
    top.set_pad(8);
    top.set_margin(8);
    top.begin();

    /* Repository */
    let w_url;
    let w_password;
    {
        let mut pack = Pack::default().size_of_parent().with_type(PackType::Vertical);
        pack.set_spacing(4);
        pack.begin();

        let mut frame = Frame::default().with_size(window.width(), 20).with_label("Repository");
        frame.set_frame(FrameType::BorderBox);

        let mut row = Flex::default().with_size(window.width(), 40).row();
        row.begin();
        let label = Frame::default().with_size(window.width(), 20).with_label("URL");
        row.fixed(&label, 80);
        w_url = Input::default().with_size(window.width(), 40);
        row.end();

        let mut row = Flex::default().with_size(window.width(), 40).row();
        row.begin();
        let label = Frame::default().with_size(window.width(), 20).with_label("Password");
        row.fixed(&label, 80);
        w_password = SecretInput::default().with_size(window.width(), 40);
        row.end();

        pack.end();
    }

    /* User */
    let w_nickname;
    {
        let mut pack = Pack::default().size_of_parent().with_type(PackType::Vertical);
        pack.set_spacing(4);
        pack.begin();

        let mut frame = Frame::default().with_size(window.width(), 20).with_label("User");
        frame.set_frame(FrameType::BorderBox);

        let mut row = Flex::default().with_size(window.width(), 40).row();
        row.begin();
        let label = Frame::default().with_size(window.width(), 20).with_label("Nickname");
        row.fixed(&label, 80);
        w_nickname = Input::default().with_size(window.width(), 40);
        row.end();

        pack.end();
    }

    /* Hardware */
    let mut w_screen;
    let w_width;
    let w_height;
    let w_rate;
    let screens = windows::screen::get().unwrap_or_default();
    {
        let mut pack = Pack::default().size_of_parent().with_type(PackType::Vertical);
        pack.set_spacing(4);
        pack.begin();

        let mut frame = Frame::default().with_size(window.width(), 20).with_label("Display");
        frame.set_frame(FrameType::BorderBox);

        Frame::default()
            .with_size(window.width(), 20)
            .with_label("Select your primary display");

        w_screen = Choice::default().with_size(window.width(), 40);
        for screen in screens.iter() {
            w_screen.add_choice(&format!("{}", screen));
        }
        w_screen.set_value(0);

        let mut row = Flex::default().with_size(window.width(), 40).row();
        row.begin();
        let frame = Frame::default().with_size(window.width(), 20).with_label("Width");
        row.fixed(&frame, 50);
        w_width = IntInput::default().with_size(window.width(), 20);

        let frame = Frame::default().with_size(window.width(), 20).with_label("Height");
        row.fixed(&frame, 50);
        w_height = IntInput::default().with_size(window.width(), 20);

        let frame = Frame::default()
            .with_size(window.width(), 20)
            .with_label("Refresh rate");
        row.fixed(&frame, 90);
        w_rate = IntInput::default().with_size(window.width(), 20);
        row.end();

        if !screens.is_empty() {
            row.deactivate();
        }

        pack.end();
    }

    /* Install */
    let install_path = Rc::new(Cell::new(default_install_path.to_owned()));
    {
        let mut pack = Pack::default().size_of_parent().with_type(PackType::Vertical);
        pack.set_spacing(4);
        pack.begin();

        let mut frame = Frame::default()
            .with_size(window.width(), 20)
            .with_label("Installation directory");
        frame.set_frame(FrameType::BorderBox);

        let mut row = Flex::default().with_size(window.width(), 40).row();
        row.begin();
        let mut input = FileInput::default().with_size(window.width(), 20);
        let mut button = Button::default().with_size(40, 40).with_label("Browse");
        row.fixed(&button, 40);
        row.end();

        let install_path = Rc::clone(&install_path);
        let default_path = default_install_path.display().to_string();
        input.set_value(&default_path);
        button.set_callback(move |_| {
            if let Some(dir) = dialog::dir_chooser("Installation directory", &default_path, false) {
                input.set_value(&dir);
                install_path.set(dir.into());
            }
        });

        pack.end();
    }

    let mut w_ok = Button::default().with_size(40, 40).with_label("Ok");
    top.fixed(&w_ok, 40);

    top.end();
    window.end();

    w_ok.set_callback({
        let mut window = window.clone();
        let output = Rc::clone(&output);
        let install_path = Rc::clone(&install_path);
        move |_| {
            let nickname = w_nickname.value().into_boxed_str();
            let url = w_url.value().into();
            let password = w_password.value().into();

            let repository = Repository {
                url,
                credentials: Some(Credentials::Basic {
                    // Username is hardcoded for now
                    username: "tagl".into(),
                    password,
                }),
            };

            let mut width = w_width.value().parse().unwrap_or(0);
            let mut height = w_height.value().parse().unwrap_or(0);
            let mut refresh_rate = w_rate.value().parse().unwrap_or(0);

            if !screens.is_empty() {
                let screen = &screens[w_screen.value() as usize];
                width = screen.width;
                height = screen.height;
                refresh_rate = screen.refresh_rate;
            }

            let config = Config {
                user: User { nickname },
                hardware: Hardware {
                    screen: Screen {
                        width,
                        height,
                        refresh_rate,
                    },
                },
                install_path: install_path.take().into(),
            };

            output.set(Some((repository, config)));
            window.hide();
        }
    });

    window.show();
    while app::wait() && window.shown() {
        trace!("waiting for window to close");
    }
    output.take()
}
