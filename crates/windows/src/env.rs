use crate::{error::Error, path::PathBuf};
use std::ptr;
use widestring::U16CString;
use windows_sys::Win32::System::Environment::GetCurrentDirectoryW;

pub fn current_dir() -> Result<PathBuf, Error> {
    // SAFETY: Win32 foreign function.
    let n = unsafe { GetCurrentDirectoryW(0, ptr::null_mut()) };
    if n == 0 {
        return Err(Error::from_win32());
    }

    let mut data = Vec::<u16>::with_capacity(n as usize);
    // SAFETY: Win32 foreign function.
    let n = unsafe { GetCurrentDirectoryW(data.capacity() as u32, data.as_mut_ptr()) };
    if n == 0 {
        return Err(Error::from_win32());
    }
    if n != (data.capacity() - 1) as u32 {
        return Err(Error::from_win32());
    }
    // SAFETY: `n + 1` characters have been written.
    unsafe {
        data.set_len((n + 1) as usize);
    }

    let s = U16CString::from_vec(data).unwrap();
    Ok(PathBuf::from(s))
}
