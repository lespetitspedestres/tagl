use crate::{
    error::Error,
    path::{Path, PathBuf},
};
use std::{
    cell::{Ref, RefCell},
    fmt, mem,
    rc::Rc,
};
use widestring::{u16cstr, U16CStr};
use windows_sys::Win32::{
    Foundation::{ERROR_FILE_NOT_FOUND, ERROR_NO_MORE_FILES, INVALID_HANDLE_VALUE, TRUE},
    Storage::FileSystem::{FindClose, FindFileHandle, FindFirstFileW, FindNextFileW, WIN32_FIND_DATAW},
};

struct WalkDirData {
    inner: WIN32_FIND_DATAW,
}

impl WalkDirData {
    fn file_name(&self) -> &U16CStr {
        // SAFETY: `cFileName` is null-terminated.
        unsafe { U16CStr::from_slice_truncate(&self.inner.cFileName).unwrap_unchecked() }
    }
}

impl fmt::Debug for WalkDirData {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("WalkDirData")
            .field("dwFileAttributes", &self.inner.dwFileAttributes)
            .field("nFileSizeHigh", &self.inner.nFileSizeHigh)
            .field("nFileSizeLow", &self.inner.nFileSizeLow)
            .field("cFileName", &self.inner.cFileName)
            .field("cAlternateFileName", &self.inner.cAlternateFileName)
            .finish()
    }
}

impl Default for WalkDirData {
    fn default() -> Self {
        // SAFETY: WIN32_FIND_DATAW is zeroable.
        let inner = unsafe { mem::zeroed() };
        Self { inner }
    }
}

fn skip(name: &U16CStr) -> bool {
    name == u16cstr!(".") || name == u16cstr!("..")
}

#[derive(Debug)]
pub struct WalkDir {
    handle: FindFileHandle,
    path: PathBuf,
    data: Rc<RefCell<WalkDirData>>,
    fuse: bool,
}

impl WalkDir {
    pub fn new(path: &Path) -> Self {
        let mut path = path.to_path_buf();
        path.push(u16cstr!("*"));
        let data = WalkDirData::default();
        Self {
            handle: INVALID_HANDLE_VALUE,
            path,
            data: Rc::new(RefCell::new(data)),
            fuse: false,
        }
    }

    fn init(&mut self) -> Option<Result<Entry, Error>> {
        let mut data = self.data.borrow_mut();
        // SAFETY: Win32 foreign function.
        self.handle = unsafe { FindFirstFileW(self.path.as_ptr(), &mut data.inner) };
        if self.handle == INVALID_HANDLE_VALUE {
            self.fuse = true;
            let err = Error::from_win32();
            if err.code().0 == ERROR_FILE_NOT_FOUND as _ {
                None
            } else {
                Some(Err(err))
            }
        } else if skip(data.file_name()) {
            drop(data);
            self.next()
        } else {
            let data = Rc::clone(&self.data);
            Some(Ok(Entry::new(data)))
        }
    }

    fn next(&mut self) -> Option<Result<Entry, Error>> {
        let mut data = self.data.borrow_mut();
        loop {
            // SAFETY: Win32 foreign function.
            let r = unsafe { FindNextFileW(self.handle, &mut data.inner) };
            if r != TRUE {
                self.fuse = true;
                let err = Error::from_win32();
                if err.code().0 == ERROR_NO_MORE_FILES as _ {
                    break None;
                } else {
                    break Some(Err(err));
                }
            }
            if !skip(data.file_name()) {
                let data = Rc::clone(&self.data);
                break Some(Ok(Entry::new(data)));
            }
        }
    }

    fn close(&mut self) {
        // SAFETY: Win32 foreign function.
        unsafe {
            FindClose(self.handle);
        }
        self.handle = INVALID_HANDLE_VALUE;
    }
}

impl Iterator for WalkDir {
    type Item = Result<Entry, Error>;

    fn next(&mut self) -> Option<Self::Item> {
        if self.fuse {
            None
        } else if self.handle == INVALID_HANDLE_VALUE {
            self.init()
        } else {
            self.next()
        }
    }
}

impl Drop for WalkDir {
    fn drop(&mut self) {
        self.close();
    }
}

#[derive(Debug)]
pub struct Entry {
    data: Rc<RefCell<WalkDirData>>,
}

impl Entry {
    fn new(data: Rc<RefCell<WalkDirData>>) -> Self {
        Self { data }
    }

    pub fn file_name(&self) -> Ref<'_, U16CStr> {
        let data = self.data.borrow();
        Ref::map(data, WalkDirData::file_name)
    }
}
