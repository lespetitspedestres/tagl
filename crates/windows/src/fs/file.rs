use crate::{error::Error, path::Path};
use std::{io, marker::PhantomData, ops, ptr, slice};
use windows_sys::Win32::Foundation::{CloseHandle, ERROR_INVALID_HANDLE, GENERIC_READ, GENERIC_WRITE, HANDLE, TRUE};
use windows_sys::Win32::Storage::FileSystem::{
    CreateFileW, FlushFileBuffers, GetFileSizeEx, ReadFile, SetEndOfFile, SetFilePointerEx, WriteFile, CREATE_ALWAYS,
    FILE_ATTRIBUTE_NORMAL, FILE_BEGIN, OPEN_EXISTING,
};
use windows_sys::Win32::System::Memory::{
    CreateFileMappingW, MapViewOfFile, UnmapViewOfFile, FILE_MAP_ALL_ACCESS, FILE_MAP_READ, MEMORYMAPPEDVIEW_HANDLE,
    PAGE_READONLY, PAGE_READWRITE,
};

/// An object providing *exclusive* access to a file, thus allowing for safe memory-mapping.
#[derive(Debug)]
pub struct File {
    handle: HANDLE,
}

impl File {
    /// Opens a file in read-only mode, with exclusive access for the current process.
    ///
    /// # Arguments
    /// * `path` - The path to the file to open.
    ///
    /// # Errors
    /// Returns an error if the file cannot be opened.
    pub fn open<P: AsRef<Path>>(path: P) -> Result<Self, Error> {
        let share_mode = 0; // Exclusive access
        let path = path.as_ref();
        // https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createfilew
        // SAFETY: Win32 foreign function.
        let handle = unsafe {
            CreateFileW(
                path.as_ptr(),
                GENERIC_READ,
                share_mode,
                ptr::null(),
                OPEN_EXISTING,
                FILE_ATTRIBUTE_NORMAL,
                0,
            )
        };

        if handle == ERROR_INVALID_HANDLE as _ {
            Err(Error::from_win32())
        } else {
            Ok(Self { handle })
        }
    }

    /// Opens a file in read-write mode, with exclusive access for the current process.
    /// If the file already exists, its contents are truncated.
    ///
    /// # Arguments
    /// * `path` - The path to the file to create.
    ///
    /// # Errors
    /// Returns an error if the file cannot be created.
    pub fn create<P: AsRef<Path>>(path: P) -> Result<Self, Error> {
        let share_mode = 0; // Exclusive access
        let path = path.as_ref();
        // https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-createfilew
        // SAFETY: Win32 foreign function.
        let handle = unsafe {
            CreateFileW(
                path.as_ptr(),
                GENERIC_READ | GENERIC_WRITE,
                share_mode,
                ptr::null(),
                CREATE_ALWAYS,
                FILE_ATTRIBUTE_NORMAL,
                0,
            )
        };

        if handle == ERROR_INVALID_HANDLE as _ {
            Err(Error::from_win32())
        } else {
            Ok(Self { handle })
        }
    }

    /// Returns the length of the file in bytes.
    pub fn len(&self) -> Result<i64, Error> {
        let mut size = 0;
        // https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-getfilesizeex
        // SAFETY: Win32 foreign function.
        let rc = unsafe { GetFileSizeEx(self.handle, &mut size) };
        if rc != TRUE {
            return Err(Error::from_win32());
        }
        Ok(size)
    }

    pub fn truncate(&mut self, size: i64) -> Result<(), Error> {
        // https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-setfilepointerex
        // SAFETY: Win32 foreign function.
        let rc = unsafe { SetFilePointerEx(self.handle, size, ptr::null_mut(), FILE_BEGIN) };
        if rc != TRUE {
            return Err(Error::from_win32());
        }
        // https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-setendoffile
        // SAFETY: Win32 foreign function.
        let rc = unsafe { SetEndOfFile(self.handle) };
        if rc != TRUE {
            return Err(Error::from_win32());
        }
        Ok(())
    }

    /// Creates a read-only memory mapping for the file.
    pub fn map(&self) -> Result<FileMap<'_>, Error> {
        // https://learn.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-createfilemappingw
        // SAFETY: Win32 foreign function.
        let handle = unsafe { CreateFileMappingW(self.handle, ptr::null(), PAGE_READONLY, 0, 0, ptr::null()) };
        if handle == 0 {
            return Err(Error::from_win32());
        }
        let size = self.len()?;
        Ok(FileMap::new(handle, size))
    }

    /// Creates a read-write memory mapping for the file.
    ///
    /// This will fail if the file was opened in read-only mode.
    pub fn map_mut(&mut self) -> Result<FileMapMut<'_>, Error> {
        // https://learn.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-createfilemappingw
        // SAFETY: Win32 foreign function.
        let handle = unsafe { CreateFileMappingW(self.handle, ptr::null(), PAGE_READWRITE, 0, 0, ptr::null()) };
        if handle == 0 {
            return Err(Error::from_win32());
        }
        let size = self.len()?;
        Ok(FileMapMut::new(handle, size))
    }
}

impl Drop for File {
    fn drop(&mut self) {
        // SAFETY: Win32 foreign function.
        let _ = unsafe { CloseHandle(self.handle) };
    }
}

impl io::Read for File {
    fn read(&mut self, buf: &mut [u8]) -> io::Result<usize> {
        let mut read = 0;
        // https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-readfile
        // SAFETY: Win32 foreign function.
        let rc = unsafe {
            ReadFile(
                self.handle,
                buf.as_mut_ptr() as _,
                buf.len() as _,
                &mut read,
                ptr::null_mut(),
            )
        };
        if rc != TRUE {
            return Err(Error::from_win32().into());
        }
        Ok(read as usize)
    }
}

impl io::Write for File {
    fn write(&mut self, _buf: &[u8]) -> io::Result<usize> {
        // https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-writefile
        // SAFETY: Win32 foreign function.
        let mut written = 0;
        let rc = unsafe {
            WriteFile(
                self.handle,
                _buf.as_ptr() as _,
                _buf.len() as _,
                &mut written,
                ptr::null_mut(),
            )
        };
        if rc != TRUE {
            return Err(Error::from_win32().into());
        }
        Ok(written as usize)
    }

    fn flush(&mut self) -> io::Result<()> {
        // https://learn.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-flushfilebuffers
        // SAFETY: Win32 foreign function.
        let rc = unsafe { FlushFileBuffers(self.handle) };
        if rc != TRUE {
            return Err(Error::from_win32().into());
        }
        Ok(())
    }
}

/// A read-only memory-mapping of a file.
#[derive(Debug)]
pub struct FileMap<'a> {
    handle: HANDLE,
    size: i64,
    _lt: PhantomData<&'a ()>,
}

impl<'a> FileMap<'a> {
    fn new(handle: HANDLE, size: i64) -> Self {
        Self {
            handle,
            size,
            _lt: PhantomData,
        }
    }

    pub fn view(&self) -> Result<FileView<'a>, Error> {
        // https://learn.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-mapviewoffile
        // SAFETY: Win32 foreign function.
        let base = unsafe { MapViewOfFile(self.handle, FILE_MAP_READ, 0, 0, 0) };
        if base == 0 {
            return Err(Error::from_win32());
        }
        Ok(FileView::new(base, self.size))
    }
}

impl Drop for FileMap<'_> {
    fn drop(&mut self) {
        // SAFETY: Win32 foreign function.
        unsafe { CloseHandle(self.handle) };
    }
}

/// A read-write memory-mapping of a file.
#[derive(Debug)]
pub struct FileMapMut<'a> {
    handle: HANDLE,
    size: i64,
    _lt: PhantomData<&'a mut ()>,
}

impl<'a> FileMapMut<'a> {
    fn new(handle: HANDLE, size: i64) -> Self {
        Self {
            handle,
            size,
            _lt: PhantomData,
        }
    }

    pub fn view_mut(&mut self) -> Result<FileViewMut<'a>, Error> {
        // https://learn.microsoft.com/en-us/windows/win32/api/memoryapi/nf-memoryapi-mapviewoffile
        // SAFETY: Win32 foreign function.
        let base = unsafe { MapViewOfFile(self.handle, FILE_MAP_ALL_ACCESS, 0, 0, 0) };
        if base == 0 {
            return Err(Error::from_win32());
        }
        Ok(FileViewMut::new(base, self.size))
    }
}

/// A read-only view of a memory-mapped file.
#[derive(Debug)]
pub struct FileView<'a> {
    base: MEMORYMAPPEDVIEW_HANDLE,
    size: i64,
    _lt: PhantomData<&'a ()>,
}

impl<'a> FileView<'a> {
    fn new(base: MEMORYMAPPEDVIEW_HANDLE, size: i64) -> Self {
        Self {
            base,
            size,
            _lt: PhantomData,
        }
    }
}

impl AsRef<[u8]> for FileView<'_> {
    fn as_ref(&self) -> &[u8] {
        &*self
    }
}

impl ops::Deref for FileView<'_> {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        // SAFETY: `self.base` is a valid pointer to `self.size` bytes.
        unsafe { slice::from_raw_parts(self.base as *const u8, self.size as usize) }
    }
}

impl Drop for FileView<'_> {
    fn drop(&mut self) {
        // SAFETY: Win32 foreign function.
        unsafe { UnmapViewOfFile(self.base) };
    }
}

/// A read-write view of a memory-mapped file.
#[derive(Debug)]
pub struct FileViewMut<'a> {
    base: MEMORYMAPPEDVIEW_HANDLE,
    size: i64,
    _lt: PhantomData<&'a mut ()>,
}

impl<'a> FileViewMut<'a> {
    fn new(base: MEMORYMAPPEDVIEW_HANDLE, size: i64) -> Self {
        Self {
            base,
            size,
            _lt: PhantomData,
        }
    }
}

impl ops::Deref for FileViewMut<'_> {
    type Target = [u8];

    fn deref(&self) -> &[u8] {
        // SAFETY: `self.base` is a valid pointer to `self.size` bytes.
        unsafe { slice::from_raw_parts(self.base as *const u8, self.size as usize) }
    }
}

impl ops::DerefMut for FileViewMut<'_> {
    fn deref_mut(&mut self) -> &mut [u8] {
        // SAFETY: `self.base` is a valid pointer to `self.size` bytes.
        unsafe { slice::from_raw_parts_mut(self.base as *mut u8, self.size as usize) }
    }
}

impl Drop for FileViewMut<'_> {
    fn drop(&mut self) {
        // SAFETY: Win32 foreign function.
        unsafe { UnmapViewOfFile(self.base) };
    }
}
