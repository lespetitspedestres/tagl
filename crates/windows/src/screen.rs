//! Access to informations about screens.
use std::{fmt, io, mem, num::NonZeroU32, ptr, ptr::addr_of_mut};
use widestring::U16CStr;
use windows_sys::Win32::Devices::Display::{
    DisplayConfigGetDeviceInfo, GetDisplayConfigBufferSizes, QueryDisplayConfig,
    DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_NAME, DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_PREFERRED_MODE,
    DISPLAYCONFIG_DEVICE_INFO_HEADER, DISPLAYCONFIG_MODE_INFO, DISPLAYCONFIG_OUTPUT_TECHNOLOGY_DISPLAYPORT_EXTERNAL,
    DISPLAYCONFIG_OUTPUT_TECHNOLOGY_DVI, DISPLAYCONFIG_OUTPUT_TECHNOLOGY_HD15, DISPLAYCONFIG_OUTPUT_TECHNOLOGY_HDMI,
    DISPLAYCONFIG_PATH_INFO, DISPLAYCONFIG_TARGET_DEVICE_NAME, DISPLAYCONFIG_TARGET_PREFERRED_MODE,
    QDC_ONLY_ACTIVE_PATHS,
};
use windows_sys::Win32::Foundation::{ERROR_INSUFFICIENT_BUFFER, ERROR_SUCCESS, TRUE};

/// Information about a screen.
#[derive(Debug, Clone)]
pub struct Screen {
    /// The name of the screen.
    pub name: Option<Box<str>>,
    /// The width in pixels.
    pub width: u32,
    /// The height in pixels.
    pub height: u32,
    /// The refresh rate in Hertz.
    pub refresh_rate: u32,
    /// The connector on which the screen is plugged-in.
    pub connector: Option<Connector>,
}

impl fmt::Display for Screen {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let name = self.name.as_deref().unwrap_or("<unknown>");
        write!(f, "\"{}\" {}x{} {}Hz", name, self.width, self.height, self.refresh_rate)?;
        if let Some(connector) = self.connector {
            write!(f, " ({})", connector)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Connector {
    kind: ConnectorKind,
    index: Option<NonZeroU32>,
}

impl fmt::Display for Connector {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.kind)?;
        if let Some(index) = self.index {
            write!(f, "-{}", index)?;
        }
        Ok(())
    }
}

#[derive(Debug, Clone, Copy)]
pub enum ConnectorKind {
    VGA,
    DVI,
    HDMI,
    DisplayPort,
    Other,
}

impl TryFrom<i32> for ConnectorKind {
    type Error = ();

    fn try_from(v: i32) -> Result<Self, Self::Error> {
        match v {
            DISPLAYCONFIG_OUTPUT_TECHNOLOGY_HD15 => Ok(Self::VGA),
            DISPLAYCONFIG_OUTPUT_TECHNOLOGY_DVI => Ok(Self::DVI),
            DISPLAYCONFIG_OUTPUT_TECHNOLOGY_HDMI => Ok(Self::HDMI),
            DISPLAYCONFIG_OUTPUT_TECHNOLOGY_DISPLAYPORT_EXTERNAL => Ok(Self::DisplayPort),
            _ => Err(()),
        }
    }
}

impl fmt::Display for ConnectorKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::VGA => write!(f, "VGA"),
            Self::DVI => write!(f, "DVI"),
            Self::HDMI => write!(f, "HDMI"),
            Self::DisplayPort => write!(f, "DP"),
            Self::Other => write!(f, "Other"),
        }
    }
}

fn get_buffer_sizes(path_count: &mut u32, mode_count: &mut u32) -> io::Result<()> {
    // https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-getdisplayconfigbuffersizes
    // SAFETY: Win32 foreign function.
    let rc = unsafe { GetDisplayConfigBufferSizes(QDC_ONLY_ACTIVE_PATHS, path_count, mode_count) };
    if rc != ERROR_SUCCESS {
        Err(io::Error::last_os_error())
    } else {
        Ok(())
    }
}

fn query_config(
    path_count: &mut u32,
    paths: &mut [DISPLAYCONFIG_PATH_INFO],
    mode_count: &mut u32,
    modes: &mut [DISPLAYCONFIG_MODE_INFO],
) -> io::Result<bool> {
    // https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-querydisplayconfig
    // SAFETY: Win32 foreign function.
    let rc = unsafe {
        QueryDisplayConfig(
            QDC_ONLY_ACTIVE_PATHS,
            path_count,
            paths.as_mut_ptr(),
            mode_count,
            modes.as_mut_ptr(),
            ptr::null_mut(),
        )
    };
    if rc != ERROR_SUCCESS {
        Err(io::Error::last_os_error())
    } else {
        Ok(rc != ERROR_INSUFFICIENT_BUFFER)
    }
}

unsafe fn get_device_info(header: *mut DISPLAYCONFIG_DEVICE_INFO_HEADER) -> io::Result<()> {
    // https://learn.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-displayconfiggetdeviceinfo
    // SAFETY: Win32 foreign function.
    let rc = unsafe { DisplayConfigGetDeviceInfo(header) };
    if rc != ERROR_SUCCESS as _ {
        Err(io::Error::last_os_error())
    } else {
        Ok(())
    }
}

fn make_target_device_name() -> DISPLAYCONFIG_TARGET_DEVICE_NAME {
    // SAFETY: DISPLAYCONFIG_TARGET_DEVICE_NAME is safe to zero initialize.
    unsafe { mem::zeroed() }
}

fn make_target_preferred_mode() -> DISPLAYCONFIG_TARGET_PREFERRED_MODE {
    // SAFETY: DISPLAYCONFIG_TARGET_PREFERRED_MODE is safe to zero initialize.
    unsafe { mem::zeroed() }
}

fn make_path_info() -> DISPLAYCONFIG_PATH_INFO {
    // SAFETY: DISPLAYCONFIG_PATH_INFO is safe to zero initialize.
    unsafe { mem::zeroed() }
}

fn make_mode_info() -> DISPLAYCONFIG_MODE_INFO {
    // SAFETY: DISPLAYCONFIG_MODE_INFO is safe to zero initialize.
    unsafe { mem::zeroed() }
}

macro_rules! init_device_info_header_target {
    ($path:ident, $info:ident, $type:ident) => {
        $info.header = DISPLAYCONFIG_DEVICE_INFO_HEADER {
            adapterId: $path.targetInfo.adapterId,
            id: $path.targetInfo.id,
            r#type: $type,
            size: mem::size_of_val(&$info) as u32,
        }
    };
}

fn get_name_and_connector(path: &DISPLAYCONFIG_PATH_INFO) -> io::Result<(Option<Box<str>>, Option<Connector>)> {
    let mut target_name = make_target_device_name();
    init_device_info_header_target!(path, target_name, DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_NAME);

    // SAFETY: Header is properly initialized.
    unsafe { get_device_info(addr_of_mut!(target_name.header))? };

    // SAFETY: Reading from union field.
    let friendly_name_from_edid = unsafe { target_name.flags.Anonymous.value & 0x1 } != 0;

    let connector = ConnectorKind::try_from(target_name.outputTechnology)
        .ok()
        .map(|kind| Connector {
            kind,
            index: NonZeroU32::new(target_name.connectorInstance),
        });

    let name = if friendly_name_from_edid {
        let name = U16CStr::from_slice_truncate(&target_name.monitorFriendlyDeviceName)
            .unwrap()
            .to_string_lossy()
            .into_boxed_str();
        Some(name)
    } else {
        None
    };

    Ok((name, connector))
}

fn get_dimensions(path: &DISPLAYCONFIG_PATH_INFO) -> io::Result<(u32, u32)> {
    let mut target_preferred_mode = make_target_preferred_mode();
    init_device_info_header_target!(
        path,
        target_preferred_mode,
        DISPLAYCONFIG_DEVICE_INFO_GET_TARGET_PREFERRED_MODE
    );
    // SAFETY: Header is properly initialized.
    unsafe { get_device_info(addr_of_mut!(target_preferred_mode.header))? };

    let width = target_preferred_mode.width;
    let height = target_preferred_mode.height;
    Ok((width, height))
}

pub fn get() -> io::Result<Vec<Screen>> {
    let mut paths: Vec<DISPLAYCONFIG_PATH_INFO> = Vec::new();
    let mut modes: Vec<DISPLAYCONFIG_MODE_INFO> = Vec::new();
    let mut screens = Vec::new();

    loop {
        let mut path_count: u32 = 0;
        let mut mode_count: u32 = 0;
        get_buffer_sizes(&mut path_count, &mut mode_count)?;

        paths.resize_with(path_count as usize, make_path_info);
        modes.resize_with(mode_count as usize, make_mode_info);

        if query_config(&mut path_count, &mut paths, &mut mode_count, &mut modes)? {
            break;
        }

        paths.resize_with(path_count as usize, make_path_info);
        modes.resize_with(mode_count as usize, make_mode_info);
    }

    for path in &paths {
        let (name, connector) = get_name_and_connector(path)?;
        let (width, height) = get_dimensions(path)?;

        let refresh_rate = {
            let r = path.targetInfo.refreshRate;
            (r.Numerator as f64 / r.Denominator as f64).ceil() as u32
        };

        assert_eq!(path.targetInfo.targetAvailable, TRUE);

        screens.push(Screen {
            name,
            width,
            height,
            refresh_rate,
            connector,
        })
    }

    Ok(screens)
}
