//! This crate provides safe wrappers for Windows APIs.
#![cfg(windows)]
#![deny(clippy::undocumented_unsafe_blocks)]
#![deny(missing_debug_implementations)]
#![deny(unsafe_op_in_unsafe_fn)]

pub mod dns;
pub mod env;
pub mod fs;
pub mod mutex;
pub mod path;
pub mod screen;

pub use windows_result as error;
