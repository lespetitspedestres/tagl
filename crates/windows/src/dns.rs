use crate::error::{Error, HRESULT};
use std::{
    cell::RefCell,
    ffi::{c_void, CStr},
    fmt, mem,
    mem::MaybeUninit,
    net::{Ipv4Addr, Ipv6Addr},
    ptr, slice,
    str::Utf8Error,
    thread,
    time::Duration,
};
use widestring::U16CString;
use windows_sys::Win32::Foundation::ERROR_SUCCESS;
use windows_sys::Win32::NetworkManagement::Dns::{
    self, DnsFree, DnsFreeRecordList, DnsQueryEx, DnsStartMulticastQuery, DnsStopMulticastQuery, DNS_RECORDA, DNS_TYPE,
};

#[derive(Debug)]
#[repr(u16)]
#[non_exhaustive]
pub enum Kind {
    A = Dns::DNS_TYPE_A,
    AAAA = Dns::DNS_TYPE_AAAA,
    Text = Dns::DNS_TYPE_TEXT,
    Ptr = Dns::DNS_TYPE_PTR,
    Srv = Dns::DNS_TYPE_SRV,
}

pub struct Response {
    head: *mut DNS_RECORDA,
    cursor: *mut DNS_RECORDA,
}

impl Default for Response {
    fn default() -> Self {
        Self {
            head: ptr::null_mut(),
            cursor: ptr::null_mut(),
        }
    }
}

impl Iterator for Response {
    type Item = Result<Answer, Utf8Error>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if self.cursor.is_null() {
                break None;
            }
            // SAFETY: pointer from Win32 assumed to be valid.
            let record = unsafe { ptr::read(self.cursor) };
            self.cursor = record.pNext;

            match record.wType {
                Dns::DNS_TYPE_A => {
                    // SAFETY: Reading from union field after tag check.
                    let ip = unsafe { record.Data.A.IpAddress };
                    let ip = Ipv4Addr::from(u32::from_be(ip));
                    break Some(Ok(Answer::A(ip)));
                }
                Dns::DNS_TYPE_AAAA => {
                    // SAFETY: Reading from union field after tag check.
                    let ip = unsafe { record.Data.AAAA.Ip6Address.IP6Byte };
                    let ip = Ipv6Addr::from(ip);
                    break Some(Ok(Answer::AAAA(ip)));
                }
                Dns::DNS_TYPE_TEXT => {
                    // SAFETY: Reading from union field after tag check.
                    let data = unsafe { record.Data.Txt };
                    let slice =
                        // SAFETY: pointer from Win32 assumed to be valid slice.
                        unsafe { slice::from_raw_parts(data.pStringArray.as_ptr(), data.dwStringCount as usize) };
                    let result: Result<String, _> = slice
                        .iter()
                        .copied()
                        .map(|ptr| {
                            // SAFETY: pointer from Win32 assumed to be valid nul-terminated string.
                            unsafe { CStr::from_ptr(ptr as *const _) }
                        })
                        .map(|txt| txt.to_str())
                        .collect();
                    break Some(result.map(|text| Answer::Txt(text.into())));
                }
                Dns::DNS_TYPE_PTR => {
                    // SAFETY: Reading from union field after tag check.
                    let data = unsafe { record.Data.Ptr };
                    // SAFETY: pointer from Win32 assumed to be valid nul-terminated string.
                    let name = unsafe { CStr::from_ptr(data.pNameHost as *const _) };
                    break Some(name.to_str().map(|name| Answer::Ptr(name.into())));
                }
                Dns::DNS_TYPE_SRV => {
                    // SAFETY: Reading from union field after tag check.
                    let data = unsafe { record.Data.Srv };
                    // SAFETY: pointer from Win32 assumed to be valid nul-terminated string.
                    let name = unsafe { CStr::from_ptr(data.pNameTarget as *const _) };
                    break Some(name.to_str().map(|name| Answer::Srv {
                        target: name.into(),
                        priority: data.wPriority,
                        weight: data.wWeight,
                        port: data.wPort,
                    }));
                }
                _ => { /* ignored */ }
            }
        }
    }
}

impl Drop for Response {
    fn drop(&mut self) {
        // SAFETY: Win32 foreign function.
        unsafe { DnsFree(self.head as *const _, DnsFreeRecordList) }
    }
}

impl fmt::Debug for Response {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Response").finish_non_exhaustive()
    }
}

#[derive(Debug, PartialEq)]
#[non_exhaustive]
pub enum Answer {
    A(Ipv4Addr),
    AAAA(Ipv6Addr),
    Txt(Box<str>),
    Ptr(Box<str>),
    Srv {
        target: Box<str>,
        priority: u16,
        weight: u16,
        port: u16,
    },
}

/// Performs a DNS query.
pub fn query(name: &str, kind: Kind) -> Result<Response, Error> {
    let name = U16CString::from_str(name).unwrap();

    let request = Dns::DNS_QUERY_REQUEST {
        Version: Dns::DNS_QUERY_REQUEST_VERSION1,
        QueryName: name.as_ptr(),
        QueryType: kind as DNS_TYPE,
        QueryOptions: Dns::DNS_QUERY_STANDARD as _,
        pDnsServerList: ptr::null_mut(),
        InterfaceIndex: 0,
        pQueryCompletionCallback: None,
        pQueryContext: ptr::null_mut(),
    };

    // SAFETY: DNS_QUERY_RESULT is safe to zero initialize.
    let mut result: Dns::DNS_QUERY_RESULT = unsafe { mem::zeroed() };
    result.Version = Dns::DNS_QUERY_RESULTS_VERSION1;

    // https://learn.microsoft.com/en-us/windows/win32/api/windns/nf-windns-dnsqueryex
    // SAFETY: Win32 foreign function.
    let rc = unsafe { DnsQueryEx(&request, &mut result, ptr::null_mut()) };

    if rc != ERROR_SUCCESS as _ {
        return Err(Error::from_hresult(HRESULT::from_win32(rc as _)));
    }

    Ok(Response {
        head: result.pQueryRecords,
        cursor: result.pQueryRecords,
    })
}

unsafe extern "system" fn callback(
    pquerycontext: *const c_void,
    pqueryhandle: *mut Dns::MDNS_QUERY_HANDLE,
    pqueryresults: *mut Dns::DNS_QUERY_RESULT,
) {
    let _handle = pqueryhandle;
    // SAFETY: correct type from `pQueryContext`.
    let response: &RefCell<Response> = unsafe { &*(pquerycontext as *const _) };
    let response = &mut *response.borrow_mut();

    // SAFETY: pointer from Win32 assumed to be valid.
    let results = unsafe { ptr::read(pqueryresults) };
    assert_eq!(results.Version, Dns::DNS_QUERY_REQUEST_VERSION1);

    if results.QueryStatus != ERROR_SUCCESS as _ || results.pQueryRecords.is_null() {
        return;
    }

    if response.head.is_null() {
        response.head = results.pQueryRecords;
        response.cursor = results.pQueryRecords;
    } else {
        // SAFETY: pointer from Win32 assumed to be valid.
        let mut cursor = unsafe { ptr::read(response.cursor) };
        assert!(cursor.pNext.is_null());
        cursor.pNext = results.pQueryRecords;
        // SAFETY: pointer from Win32 assumed to be valid.
        unsafe { ptr::write(response.cursor, cursor) };
    }

    // Advance cursor to last element
    loop {
        // SAFETY: pointer from Win32 assumed to be valid.
        let cursor = unsafe { ptr::read(response.cursor) };
        if cursor.pNext.is_null() {
            break;
        }
        response.cursor = cursor.pNext;
    }
}

/// Performs a MDNS query.
pub fn query_local(name: &str, kind: Kind, timeout: Duration) -> Result<Response, Error> {
    let name = U16CString::from_str(name).unwrap();
    let response = RefCell::new(Response::default());

    // SAFETY: MDNS_QUERY_REQUEST is safe to zero initialize.
    let mut request: Dns::MDNS_QUERY_REQUEST = unsafe { mem::zeroed() };
    request.Version = Dns::DNS_QUERY_RESULTS_VERSION1;
    request.Query = name.as_ptr();
    request.QueryType = kind as DNS_TYPE;
    request.QueryOptions = Dns::DNS_QUERY_STANDARD as _;
    request.InterfaceIndex = 0;
    request.pQueryCallback = Some(callback);
    request.pQueryContext = &response as *const _ as *mut _;

    let mut handle = MaybeUninit::uninit();

    // SAFETY: Win32 foreign function.
    let rc = unsafe {
        // https://learn.microsoft.com/en-us/windows/win32/api/windns/nf-windns-dnsstartmulticastquery
        DnsStartMulticastQuery(&request, handle.as_mut_ptr())
    };

    if rc != ERROR_SUCCESS as _ {
        return Err(Error::from_win32());
    }

    thread::sleep(timeout);

    // SAFETY: Win32 foreign function.
    let rc = unsafe {
        // https://learn.microsoft.com/en-us/windows/win32/api/windns/nf-windns-dnsstopmulticastquery
        DnsStopMulticastQuery(handle.as_mut_ptr())
    };

    if rc != ERROR_SUCCESS as _ {
        return Err(Error::from_win32());
    }

    let mut response = response.into_inner();
    // Reset cursor
    response.cursor = response.head;

    Ok(response)
}
